// @ts-check
/**
 * @type {HTMLCanvasElement}
 */
// @ts-ignore
let canvas = document.getElementById("canvas");
let ctx = canvas.getContext('2d');

let W = 600;
let H = 600;

const MODEL_MIN_X = -2;
const MODEL_MAX_X = 2;
const MODEL_MIN_Y = -2;
const MODEL_MAX_Y = 2;

let points = [];
let triangles = [];
let colors = [
    'red', 'green', 'blue', 'white',
    'orange', 'purple', 'cyan', 'yellow'
];

function initGeometry() {
    for (let x = -1; x <= 1; x += 2)
        for (let y = -1; y <= 1; y += 2)
            for (let z = -1; z <= 1; z += 2)
                points.push(new Vector(x, y, z));
    for (let dimension = 0; dimension <= 2; dimension++)
        for (let side = -1; side <= 1; side += 2) {
            let sidePoints = points.filter(point => point[dimension] == side);
            let a = sidePoints[0];
            let b = sidePoints[1];
            let c = sidePoints[2];
            let d = sidePoints[3];
            triangles.push(makeTriangle(a, b, c, dimension, side));
            triangles.push(makeTriangle(d, b, c, dimension, side));
        }
}

function makeTriangle(a, b, c, dimension, side) {
    let side1 = b.subtract(a);
    let side2 = c.subtract(a);
    let orientationVector = side1.cross(side2);

    if (Math.sign(orientationVector[dimension]) == Math.sign(side))
        return [a, b, c];
    else return [a, c, b];
}

function perspectiveProjection(point) {
    let x = point[0];
    let y = point[1];
    let z = point[2];
    return new Vector(
        x / (z + 5),
        y / (z + 5),
        z
    );
}

function project(point) {
    let perspectivePoint = perspectiveProjection(point);
    let x = perspectivePoint[0];
    let y = perspectivePoint[1];
    let z = perspectivePoint[2];

    return new Vector(
        W * (x - MODEL_MIN_X) / (MODEL_MAX_X - MODEL_MIN_X),
        H * (1 - (y - MODEL_MIN_Y) / (MODEL_MAX_Y - MODEL_MIN_Y)),
        z
    );
}

function renderPoint(point) {
    let projectedPoint = project(point);
    let x = projectedPoint[0];
    let y = projectedPoint[1];

    ctx.beginPath();
    ctx.ellipse(x, y, 0.5, 0.5, 0, 0, 360);
    ctx.stroke();
}

function renderTriangle(triangle, color) {
    let projectedTriangle = triangle.map(project);
    let a = projectedTriangle[0];
    let b = projectedTriangle[1];
    let c = projectedTriangle[2];
    let side1 = b.subtract(a);
    let side2 = c.subtract(a);

    if (side1.ccw(side2)) {
        ctx.beginPath();
        ctx.moveTo(a[0], a[1]);
        ctx.lineTo(b[0], b[1]);
        ctx.lineTo(c[0], c[1]);
        ctx.lineTo(a[0], a[1]);
        ctx.strokeStyle = 'black';
        ctx.fillStyle = color;
        ctx.stroke();
        ctx.fill();
    }
}

let lastTime = Date.now();

let theta = 0;
let deltaTheta = 0.03;

function render() {

    let currentTime = Date.now();
    if (currentTime - lastTime > 1000 / 30) {
        theta += deltaTheta;
        lastTime = currentTime;
        ctx.fillStyle = 'black';
        ctx.fillRect(0, 0, W, H);

        ctx.fillStyle = 'white';
        ctx.strokeStyle = 'white';
        /* points.forEach(point => {
            point = point.rotateY(theta);
            point = point.rotateX(theta);
            renderPoint(point);
        }); */
        triangles.forEach((triangle, idx) => {
            let rotatedTriangle = triangle.map(point => {
                point = point.rotateY(theta);
                point = point.rotateX(theta);
                return point;
            });
            let color = colors[Math.floor(idx / 2)];
            renderTriangle(rotatedTriangle, color);
        });
    }

    requestAnimationFrame(render);
}

initGeometry();
render();
